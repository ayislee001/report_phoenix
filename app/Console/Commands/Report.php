<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use Carbon;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Report extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Report';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This is for report excel';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        ini_set("memory_limit", -1);
        set_time_limit(0);

        printf('Loading data ...').PHP_EOL;
        $stocks = DB::table('report_stock')
            ->where('created_at','>','2020-04-01')
            ->where('created_at','<','2020-04-10')
            ->get();
        print_r('Done....');

        print_r('Generating report .... ');        

        $download_report = env('DOWNLOAD_REPORT', 'c:/download/');
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle("Product Stock");

            
        $startRow = 1;
        foreach ($stocks as $data) {
            $startRow++;
            $sheet->setCellValue('A'.$startRow,$data->created_at);
            $sheet->setCellValue('B'.$startRow,'Team Leader');
            $sheet->setCellValue('C'.$startRow,$data->merchandiser);
            $sheet->setCellValue('D'.$startRow,$data->DSO);
            $sheet->setCellValue('E'.$startRow,$data->type);
            $sheet->setCellValue('F'.$startRow,$data->chain);
            $sheet->setCellValue('G'.$startRow,$data->code);
            $sheet->setCellValue('H'.$startRow,$data->store);
            $sheet->setCellValue('I'.$startRow,$data->product);
            $sheet->setCellValue('J'.$startRow,$data->stock);
            $sheet->setCellValue('K'.$startRow,$data->satuan);
            # code...
            // if($startRow>100) break;
            print_r($startRow.'_');
        }

        print_r('_--------selesai ----');
        print_r('_--------writing file ----');

        $writer = new Xlsx($spreadsheet);
        $fileDownloadName = $download_report.'repp.xlsx';
        $writer->save($fileDownloadName);
        print_r('_--------writing file selesai----');        

    }
}
